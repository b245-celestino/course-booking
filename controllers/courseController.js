const { request, response } = require("express");
const admin = require("../model/usersSchema");
const Course = require("../model/coursesSchema");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) => {
  const adminData = auth.decode(request.headers.authorization);
  let input = request.body;
  console.log(adminData);
  if (adminData.isAdmin == true) {
    let newCourse = new Course({
      name: input.name,
      description: input.description,
      price: input.price,
    });

    return (
      newCourse
        .save()
        .then((course) => {
          response.send("Successfully added");
        })
        // course creation failed
        .catch((error) => {
          console.log(error);
          response.send(false);
        })
    );
  } else {
    return response.send("You are not an admin");
  }
};

// Create a controller wherein it will retrieved all the courses (active/inactive courses)

module.exports.allCourses = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  if (!userData.isAdmin) {
    return response.send("You don't have access to this route");
  } else {
    Course.find({})
      .then((result) => response.send(result))
      .catch((error) => response.send(error));
  }
};

// Create a controller wherein it will retrieve course that are active
module.exports.allActiveCourses = (request, response) => {
  Course.find({ isActive: true })
    .then((result) => response.send(result))
    .catch((error) => response.send(error));
};

//Retrieve all inactive courses
module.exports.inActiveCourse = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  if (!userData.isAdmin) {
    return response.send("You don't have access to this route");
  } else {
    Course.find({ isActive: false })
      .then((result) => response.send(result))
      .catch((error) => response.send(error));
  }
};

// This controller will get the details of specific course
module.exports.courseDetails = (request, response) => {
  // to get the params from the url
  const courseId = request.params.courseId;
  Course.findById(courseId)
    .then((result) => response.send(result))
    .catch((error) => response.send(error));
};

// This controller is for updating specific course
module.exports.updateCourse = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const courseId = request.params.courseId;
  const input = request.body;
  if (!userData.isAdmin) {
    return response.send("You don't have access in this page!");
  } else {
    Course.findById(courseId)
      .then((result) => {
        if (result === null) {
          return response.send("CourseId is invalid, please try again");
        } else {
          let updatedCourse = {
            name: input.name,
            description: input.description,
            price: input.price,
          };
          Course.findByIdAndUpdate(courseId, updatedCourse, { new: true })
            .then((result) => {
              console.log(result);
              return response.send(result);
            })
            .catch((error) => response.send(error));
        }
      })
      .catch((error) => response.send(error));
  }
};
// Archiving Courses
module.exports.archiveCourse = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const courseId = request.params.courseId;
  if (!userData.isAdmin) {
    return response.send("You don't have access in this page!");
  } else {
    Course.findByIdAndUpdate(courseId, { isActive: false }, { new: true })
      .then((result) =>
        response.send(`The ${result.name} course has successfully Archived!`)
      )
      .catch((result) => response.send(error));
  }
};
