const mongoose = require("mongoose");
const User = require("../model/usersSchema");
const Course = require("../model/coursesSchema");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// controllers

// This controller will create or register a user on our DAtabase

module.exports.userRegistration = (request, response) => {
  const input = request.body;
  User.findOne({ email: input.email })
    .then((result) => {
      if (result !== null) {
        return response.send("The email  is already existed!");
      } else {
        let newUser = new User({
          firstName: input.firstName,
          lastName: input.lastName,
          email: input.email,
          password: bcrypt.hashSync(input.password, 10),
          mobileNo: input.mobileNo,
        });

        newUser
          .save()
          .then((save) => {
            return response.send("You are now registered to our website");
          })
          .catch((error) => {
            return response.send(error);
          });
      }
    })
    .catch((error) => {
      return response.send(error);
    });
};

// User Authentication
module.exports.userAuthentication = (request, response) => {
  let input = request.body;

  //Possible scenarios in logging in
  //1.email is not yet registered
  //2. email is registered but the password is wrong

  User.findOne({ email: input.email })
    .then((result) => {
      if (result === null) {
        return response.send(
          "Email is not yet registered. Register first before logging in"
        );
      } else {
        // "compareSync()" method used to compare a non encrypted password to the encrypted password.
        // it returns boolean value, if match true value will return otherwise false.
        const isPasswordCorrect = bcrypt.compareSync(
          input.password,
          result.password
        );

        if (isPasswordCorrect) {
          return response.send({ auth: auth.createAccessToken(result) });
        } else {
          return response.send("Password  is incorrect!");
        }
      }
    })
    .catch((error) => {
      return response.send(error);
    });
};
//retrieving the details of the user

module.exports.getProfile = (request, response) => {
  // let input = request.body;
  const userData = auth.decode(request.headers.authorization);

  // console.log(userData);

  User.findById(userData._id)
    .then((result) => {
      result.password = "";
      return response.send(result);
    })
    .catch((error) => {
      return response.send(error);
    });
};

// Controller for user enrollment
module.exports.enrollCourse = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const courseId = request.params.courseId;
  let isUserUpdated = await User.findById(userData._id)
    .then((result) => {
      if (result === null) {
        return false;
      } else if (userData.isAdmin === true) {
        return false;
      } else {
        result.enrollments.push({ courseId: courseId });
        return result
          .save()
          .then((save) => {
            return true;
          })
          .catch((error) => {
            return false;
          });
      }
    })
    .catch((error) => {
      return response.send(error);
    });

  let isCourseUpdated = await Course.findById(courseId)
    .then((result) => {
      if (result === null) {
        return false;
      } else if (result.courseId === undefined) {
        return false;
      } else {
        result.enrollees.push({ userId: userData._id });
        return result
          .save()
          .then((save) => {
            return true;
          })
          .catch((error) => {
            return false;
          });
      }
    })
    .catch((error) => {
      return response.send(
        "There was an error during the enrollment. Please try again!"
      );
    });

  console.log(isCourseUpdated);
  console.log(isUserUpdated);

  if (isCourseUpdated === true && isUserUpdated === true) {
    return response.send("The course is now enrolled!");
  } else if (isUserUpdated === false) {
    return response.send("Admin is not allowed to enroll!");
  } else {
    return response.send(
      "There was an error during the enrollment. Please try again!"
    );
  }
};
