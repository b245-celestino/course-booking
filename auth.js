const jwt = require("jsonwebtoken"); // used in the algo for encrypting our data which makes it difficult to decode the information without defined secret keyword.

const secret = "CourseBookingAPI";
//JWT is a way of securely passing the server to the frontend or the other part of the server
// information is kept secure through the use of the secret code that can decode the encrypted information

// Token Creation

//the argument that will be passed to our parameter(user) will be the document/information of our user
module.exports.createAccessToken = (user) => {
  // payload
  //will contain the data that will be passed to other parts of our API
  const data = {
    _id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  // .sign() from jwt package will generate a JSON web token
  // syntax
  //jwt.sign(payload,secretCode,options)
  return jwt.sign(data, secret);
};

// Token verification
// next function indicates that we may proceed with the next step
module.exports.verify = (request, response, next) => {
  let token = request.headers.authorization;

  // Token received and is not undefined.

  if (typeof token !== "undefined") {
    // retrieves only token and remove the "bearer" prefix
    token = token.slice(7, token.length);
    console.log(token);

    // validate the token using the "verify" method decrypting the token using the secret code.
    //jwt.verify(token,secretOrPrivateKey, [options/callbackFunction])

    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return response.send({ auth: "Failed" });
      } else {
        // the verify method will be used as middleware in the route to verify the token before proceeding to the function that invokes the controller
        next();
      }
    });
  } else {
    return response.send({ auth: "Failed" });
  }
};

//Token decryption

module.exports.decode = (token) => {
  // token received is not undefined
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        // the "decode" method is used to obtain information from the JWT.
        // jwt.decode(token,[options])
        // returns an object with access to the "payload" property which contains user information stored when the token was generated
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    // token does not exist (undefined)
    return null;
  }
};
