const { application } = require("express");
const express = require("express");
const mongoose = require("mongoose");
// by default our backend's CORS setting will prevent any application outside our Express JS app to process the request. Using the cors package, it will allow us to manipulate this and control what applications may use our app.

//Allows our backend application to be available to our frontend application
//Allows us to control the app's Cross Origin Resource Sharing
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const port = 3001;
const app = express();

//Mongoose connection
mongoose.set("strictQuery", true);
mongoose.connect(
  "mongodb+srv://admin:admin@batch245-celestino.ldu2t4p.mongodb.net/batch245_Course_API_celestino?retryWrites=true&w=majority",
  {
    //Allows us to avoid any current and future errors while connecting to MONGODB
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));

db.once("open", () => {
  console.log("We are now connected to the cloud!");
});

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use("/user", userRoutes);
app.use("/course", courseRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}!`));
