const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");

//Routes

//this is responsible for the registration of the user
router.post("/register", userController.userRegistration);

router.post("/login", userController.userAuthentication);
//this is responsible for retrieving the details of the user
router.get("/details", auth.verify, userController.getProfile);
//route for user enrollment
router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);

module.exports = router;
