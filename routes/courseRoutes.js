const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// [Route without params]
//Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

// Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourses);

router.get("/allActive", courseController.allActiveCourses);

router.get("/allinactive", auth.verify, courseController.inActiveCourse);

// [Route with params]
// Route for retrieving details of specific course
router.get("/:courseId", courseController.courseDetails);
// route for updating a course
router.put("/update/:courseId", auth.verify, courseController.updateCourse);
// route for archiving courses
router.patch("/:courseId/archive", auth.verify, courseController.archiveCourse);

module.exports = router;
